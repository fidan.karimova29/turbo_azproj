package ms24.turboaz.dto.enums;

public enum CountOfOwners {
    First,
    Second,
    Third,
    More;
}
