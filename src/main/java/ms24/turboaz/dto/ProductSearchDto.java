package ms24.turboaz.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductSearchDto {
    private String brandName;
    private String modelName;
    private String city;
    private double minPrice;
    private double maxPrice;

}
