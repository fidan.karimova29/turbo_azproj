package ms24.turboaz.dto;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ms24.turboaz.entity.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class ProductResponseDto {
    private Long id;
    private String name;
    private Integer graduationYear;
    private Integer engine;
    private String march;
    private double price;
    private LocalDateTime creationDateTime;
   //private Brand brand;
   private Model model;
    //private List<Image> images;
    private Owner owner;
    private ProductDetails productDetails;

}
