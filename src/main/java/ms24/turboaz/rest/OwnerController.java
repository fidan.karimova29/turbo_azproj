package ms24.turboaz.rest;

import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.BrandDto;
import ms24.turboaz.dto.BrandRequestDto;
import ms24.turboaz.dto.OwnerDto;
import ms24.turboaz.dto.OwnerRequestDto;
import ms24.turboaz.service.impl.BrandServiceImpl;
import ms24.turboaz.service.impl.OwnerServiceImpl;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/owner")
@RequiredArgsConstructor
public class OwnerController {
    private final OwnerServiceImpl ownerServiceImpl;

    @PostMapping
    public OwnerDto create(@RequestBody @Validated OwnerRequestDto ownerRequestDto) {
        return ownerServiceImpl.create(ownerRequestDto);
    }

    @PutMapping("/{id}")
    public OwnerDto update(@PathVariable Long id, @RequestBody OwnerRequestDto ownerRequestDto) {
        return ownerServiceImpl.update(id, ownerRequestDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        ownerServiceImpl.delete(id);
    }

    @GetMapping("/{id}")
    public OwnerDto getElementById(@PathVariable Long id) {
        return ownerServiceImpl.getElementById(id);
    }
    @GetMapping
    List<OwnerDto> getAllOwners() {
        return ownerServiceImpl.getAllOwners();
    }
}
