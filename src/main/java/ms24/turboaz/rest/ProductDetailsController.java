package ms24.turboaz.rest;

import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.*;
import ms24.turboaz.service.impl.OwnerServiceImpl;
import ms24.turboaz.service.impl.ProductDetailsServiceImpl;
import ms24.turboaz.service.impl.ProductServiceImpl;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/productDetails")
@RequiredArgsConstructor
public class ProductDetailsController {
    private final ProductDetailsServiceImpl productDetailsServiceImpl;

    @PostMapping
    public ProductDetailsResponseDto create(@RequestBody ProductDetailsRequestDto productDetailsRequestDto) {
        return productDetailsServiceImpl.create(productDetailsRequestDto);
    }

    @PutMapping("/{id}")
    public ProductDetailsResponseDto update(@PathVariable Long id, @RequestBody ProductDetailsRequestDto productDetailsRequestDto) {
        return productDetailsServiceImpl.update(id, productDetailsRequestDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        productDetailsServiceImpl.delete(id);
    }

    @GetMapping("/{id}")
    public ProductDetailsResponseDto getElementById(@PathVariable Long id) {
        return productDetailsServiceImpl.getElementById(id);
    }
    @GetMapping
    List<ProductDetailsResponseDto> getAllProducts() {
        return productDetailsServiceImpl.getAllProductDetails();
    }
}
