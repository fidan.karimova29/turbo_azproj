package ms24.turboaz.service.interfaces;

import ms24.turboaz.dto.BrandDto;
import ms24.turboaz.dto.BrandRequestDto;

import java.util.List;

public interface BrandService {
    BrandDto create(BrandRequestDto brandRequestDto);
    BrandDto update(Long id,BrandRequestDto brandRequestDto);
    void delete(Long id);
    BrandDto getElementById(Long id);
    List<BrandDto> getAllBrands();
}
