package ms24.turboaz.service.impl;

import jakarta.persistence.*;
import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.BrandDto;
import ms24.turboaz.dto.BrandRequestDto;
import ms24.turboaz.entity.Brand;
import ms24.turboaz.entity.Model;
import ms24.turboaz.repo.BrandRepository;
import ms24.turboaz.service.interfaces.BrandService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
@Service
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService {
    private final ModelMapper modelMapper;
    private final BrandRepository brandRepository;

    @Override
    public BrandDto create(BrandRequestDto brandRequestDto) {
        Brand brand = modelMapper.map(brandRequestDto, Brand.class);
        Brand brandInRepo = brandRepository.save(brand);
        return modelMapper.map(brandInRepo,BrandDto.class);
    }

    @Override
    public BrandDto update(Long id, BrandRequestDto brandRequestDto) {
        Brand brandEntity = brandRepository.findById(id).orElseThrow(()
                -> new RuntimeException("brand is not found with id " + id));
        brandEntity.setBrandName(brandRequestDto.getBrandName());
        brandEntity.setId(brandRequestDto.getId());
        return modelMapper.map(brandEntity,BrandDto.class);
    }

    @Override
    public void delete(Long id) {
        Brand brandEntity = brandRepository.findById(id).orElseThrow(()
                -> new RuntimeException("brand is not found with id " + id));
        brandRepository.delete(brandEntity);

    }

    @Override
    public BrandDto getElementById(Long id) {
        Brand brandEntity = brandRepository.findById(id).orElseThrow(()
                -> new RuntimeException("brand is not found with id " + id));
        return modelMapper.map(brandEntity,BrandDto.class);
    }

    @Override
    public List<BrandDto> getAllBrands() {
        List<Brand> all = brandRepository.findAll();
        return all.stream().map(brand->
                modelMapper.map(brand,BrandDto.class)).toList();
    }
}

