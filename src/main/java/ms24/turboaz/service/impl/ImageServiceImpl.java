package ms24.turboaz.service.impl;


import lombok.RequiredArgsConstructor;
import ms24.turboaz.dto.BrandDto;
import ms24.turboaz.dto.ImageDto;
import ms24.turboaz.dto.ImageRequestDto;
import ms24.turboaz.entity.Brand;
import ms24.turboaz.entity.Image;
import ms24.turboaz.entity.Product;
import ms24.turboaz.repo.BrandRepository;
import ms24.turboaz.repo.ImageRepository;
import ms24.turboaz.repo.ProductRepository;
import ms24.turboaz.service.interfaces.ImageService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {
    private final ModelMapper modelMapper;
    private final ImageRepository imageRepository;
    private final ProductRepository productRepository;

    @Override
    public ImageDto create(ImageRequestDto imageRequestDto) {
        Image image = modelMapper.map(imageRequestDto, Image.class);
        Product productById = findProductById(imageRequestDto.getProduct_id());
        image.setProduct(productById);
        Image imageInRepo = imageRepository.save(image);
        return modelMapper.map(imageInRepo, ImageDto.class);
    }
    private Product findProductById(Long id){
        Product product = productRepository.findById(id).orElseThrow(()
                -> new RuntimeException("product is not found with id " + id));
        return product;
    }

    @Override
    public ImageDto update(Long id, ImageRequestDto imageRequestDto) {
        Image imageEntity = imageRepository.findById(id).orElseThrow(()
                -> new RuntimeException("image is not found with id " + id));
        imageEntity.setImagePath(imageRequestDto.getImagePath());
        return modelMapper.map(imageEntity,ImageDto.class);
    }

    @Override
    public void delete(Long id) {
        Image imageEntity = imageRepository.findById(id).orElseThrow(()
                -> new RuntimeException("image is not found with id " + id));
        imageRepository.delete(imageEntity);

    }

    @Override
    public ImageDto getElementById(Long id) {
        Image imageEntity = imageRepository.findById(id).orElseThrow(()
                -> new RuntimeException("user is not found with id " + id));
        return modelMapper.map(imageEntity,ImageDto.class);
    }

    @Override
    public List<ImageDto> getAllImages() {
        List<Image> all = imageRepository.findAll();
        return all.stream().map(image->
                modelMapper.map(image,ImageDto.class)).toList();
    }
}

