package ms24.turboaz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurboazApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurboazApplication.class, args);
	}

}
