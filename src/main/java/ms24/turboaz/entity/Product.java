package ms24.turboaz.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants

public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private LocalDateTime creationDateTime = LocalDateTime.now();
    private String description;
    private double price;

    @OneToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    private List<Image> images;

    @ManyToOne(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    private Owner owner;

    @OneToOne()
    private ProductDetails productDetails;

    @ManyToOne(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    private Model model;


    @PostPersist
    public void incrementOwnerProductCount() {
        if (owner.getCountOfProducts() != null)
            owner.setCountOfProducts(owner.getCountOfProducts() + 1);
    }


}
